 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntua.google_web;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gdata.data.contacts.ContactEntry;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.VCARD;
import com.ntua.google_web.rest.serviceResource.User;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import virtuoso.jena.driver.VirtDataset;

/**
 *
 * @author stavroula
 */
public class SnUserOntology {

    //Variables for describing entry's info
    String fullName = "";
    String firstName = "";
    String lastName = "";
    String skypeId = "";
    String mail = "";
    String homepageURL = "";
    String nickName = "";
    String phone = "";
    String birthday = "";
    String streetAddress = "";
    String region = "";
    String gender = "";
    String orgName = "";
    String orgUnit = "";
    String orgTitle = "";
    String orgJobDescription = "";
    //Variables for describing ontology           
    public static String lz = "http://www.linkzoo.gr/#";
    public static String org = "http://www.w3.org/ns/org#";
    public static String foaf = "http://xmlns.com/foaf/spec/";

    public OntModel create(ContactEntry entry, String nameUser) throws FileNotFoundException, IOException {

        String url = "jdbc:virtuoso://localhost:1111/charset=UTF-8/log_enable=2";

        // create contacts model
        OntModel SnUserModel = ModelFactory.createOntologyModel();

        //create Properties
        Property lzResourceType = SnUserModel.createDatatypeProperty(lz + "resourceType");
        Property orgHasMembership = SnUserModel.createObjectProperty(org + "hasMembership");
        Property orgRole = SnUserModel.createDatatypeProperty(org + "role");
        Property orgOrganization = SnUserModel.createDatatypeProperty(org + "organization");
        Property SKYPEID = SnUserModel.createDatatypeProperty("foaf:skypeID");

        //create Resources
        Resource orgMembership = SnUserModel.createResource(org + "Membership");

        //create Contacts class, our basic class
        OntClass SnUser = SnUserModel.createClass(lz + "SnUser");
        SnUser.addLabel("Contacts", "en");
        SnUser.addComment("The class for describing a social network's user", "en");
        SnUser.addSuperClass(FOAF.Person);
        SnUser.addProperty(lzResourceType, lz + "Person");

        if (entry.hasName()) {
            com.google.gdata.data.extensions.Name name = entry.getName();
            if (name.hasFullName()) {
                fullName = entry.getName().getFullName().getValue();
            }
            if (name.hasGivenName()) {
                firstName = entry.getName().getGivenName().getValue();
            }
            if (name.hasFamilyName()) {
                lastName = entry.getName().getFamilyName().getValue();
            }
            if (entry.hasNickname()) {
                nickName = entry.getNickname().getValue();
            }
        }
        //skypeID**
        if (entry.hasImAddresses()) {
            skypeId = entry.getImAddresses().get(0).getAddress();
        }

        //mail info**
        if (entry.hasEmailAddresses()) {
            mail = entry.getEmailAddresses().get(0).getAddress();
        }

        //homepage**
        if (entry.hasWebsites()) {
            homepageURL = entry.getWebsites().get(0).getHref();
        }

        //telephone
        if (entry.hasPhoneNumbers()) {
            phone = entry.getPhoneNumbers().get(0).getPhoneNumber();
        }
        //birthday**
        if (entry.hasBirthday()) {
            birthday = entry.getBirthday().getWhen();
        }

        //street address**
        if (entry.hasStructuredPostalAddresses()) {
            List<com.google.gdata.data.extensions.StructuredPostalAddress> address = entry.getStructuredPostalAddresses();
            if (address.get(0).hasFormattedAddress()) {
                streetAddress = entry.getStructuredPostalAddresses().get(0).getFormattedAddress().getValue();
            }
            if (address.get(0).hasRegion()) {
                region = entry.getStructuredPostalAddresses().get(0).getRegion().getValue();
            }
        }

        //gender
        if (entry.hasGender()) {
            gender = entry.getGender().getValue().toString();
        }

        //job
        if (entry.hasOrganizations()) {
            List<com.google.gdata.data.extensions.Organization> org = entry.getOrganizations();
            if (org.get(0).hasOrgName()) {
                orgName = entry.getOrganizations().get(0).getOrgName().getValue();
            }
            if (org.get(0).hasOrgDepartment()) {
                orgUnit = entry.getOrganizations().get(0).getOrgDepartment().getValue();
            }
            if (org.get(0).hasOrgTitle()) {
                orgTitle = entry.getOrganizations().get(0).getOrgTitle().getValue();
            }
            if (org.get(0).hasOrgJobDescription()) {
                orgJobDescription = entry.getOrganizations().get(0).getOrgJobDescription().getValue();
            }
        }

        Resource snUser = SnUserModel.createResource(lz + fullName.replaceAll("\\s", ""));
        Resource meUser = SnUserModel.createResource(lz + nameUser.replaceAll("\\s", ""));
        SnUserModel.add(meUser, FOAF.knows, snUser);
        snUser.addProperty(FOAF.firstName, firstName);
        snUser.addProperty(FOAF.family_name, lastName);
        snUser.addProperty(FOAF.nick, nickName);
        snUser.addProperty(SKYPEID, skypeId);
        snUser.addProperty(FOAF.mbox, mail);
        snUser.addProperty(FOAF.homepage, homepageURL);
        snUser.addProperty(FOAF.phone, phone);
        snUser.addProperty(FOAF.birthday, birthday);
        snUser.addProperty(VCARD.Street, streetAddress);
        snUser.addProperty(VCARD.Region, region);
        snUser.addProperty(FOAF.gender, gender);
        snUser.addProperty(orgHasMembership, org + "Membership");
        snUser.addProperty(orgRole, orgTitle);
        snUser.addProperty(orgOrganization, orgName);

        return SnUserModel;
    }

    public void linkedin(User linkedinInfo) throws FileNotFoundException, IOException {
        String fullName_linkedin = linkedinInfo.firstName + linkedinInfo.lastName;
        OntModel SnUserModel = ModelFactory.createOntologyModel();
        Resource snUser = SnUserModel.createResource(lz + fullName_linkedin);
        Property industry = SnUserModel.createDatatypeProperty(lz + "industry");
        Property summary = SnUserModel.createDatatypeProperty(lz + "summary");
        Property headline = SnUserModel.createDatatypeProperty(lz + "headline");
        Property linkedinURL = SnUserModel.createObjectProperty(lz + "linkedinURL");
        Resource linkedinProfileUrl = SnUserModel.createResource(linkedinInfo.publicProfileUrl);
        snUser.addProperty(industry, linkedinInfo.industry);
        snUser.addProperty(summary, linkedinInfo.summary);
        snUser.addProperty(headline, linkedinInfo.headline);
        snUser.addProperty(linkedinURL, linkedinProfileUrl);
        /*FileOutputStream out = null;
        File file;
        file = new File("c:/linkedin.rdf");
        out = new FileOutputStream(file);

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }
        SnUserModel.write(out);
        out.flush();
        out.close();*/

    }
}
