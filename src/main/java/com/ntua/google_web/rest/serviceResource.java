/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntua.google_web.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gdata.util.ServiceException;
import com.hp.hpl.jena.ontology.OntModel;
import com.ntua.google_web.SNCommunicator;
import com.ntua.google_web.SnUserOntology;
import com.sun.jersey.core.spi.factory.ResponseBuilderImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * REST Web Service
 *
 * @author stavroula
 */
@Path("service")
public class serviceResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of serviceResource
     */
    public serviceResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.ntua.google_web.rest.serviceResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/html")
    public String getHtml() throws ServiceException, IOException {
        return "service!!";

    }

    @GET
    @Path("/contacts")
    @Produces(MediaType.APPLICATION_XML)
    public String getHtmlContacts() throws ServiceException, IOException {
        SNCommunicator help = new SNCommunicator();
        return help.contactsService();
    }

    @GET
    @Path("/calendar")
    @Produces(MediaType.APPLICATION_XML)
    public String getHtmlCalendar() throws IOException {
        SNCommunicator help = new SNCommunicator();
        return help.calendarService();
    }

    @GET
    @Path("/youtube")
    @Produces(MediaType.APPLICATION_XML)
    public String getHtmlYoutube() throws IOException {
        SNCommunicator help = new SNCommunicator();
        return help.youtubeService();
    }

    /**
     * PUT method for updating or creating an instance of serviceResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }

    public static class User {

        @JsonProperty("_key")
        public String key;
        @JsonProperty("headline")
        public String headline;
        @JsonProperty("industry")
        public String industry;
        @JsonProperty("publicProfileUrl")
        public String publicProfileUrl;
        @JsonProperty("summary")
        public String summary;
        @JsonProperty("firstName")
        public String firstName;
        @JsonProperty("lastName")
        public String lastName;

        //getters and setters
        public User() {
        }
    }

    @POST
    @Path("/linkedin")
    public void postLinkedin(String test) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        User linkedinInfo = mapper.readValue(test, User.class);
        SnUserOntology snUserOntology = new SnUserOntology();

        snUserOntology.linkedin(linkedinInfo);
    }
}
