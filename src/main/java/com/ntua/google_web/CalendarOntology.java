/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntua.google_web;

import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import java.util.List;
import virtuoso.jena.driver.VirtDataset;

/**
 *
 * @author stavroula
 */
public class CalendarOntology {

    public static String lz = "http://www.linkzoo.gr/#";
    public String ncal = "http://www.w3.org/2002/12/cal/icaltzd/#";

    public OntModel create(Event event) {
       // String url = "jdbc:virtuoso://localhost:1111/autoReconnect=true/charset=UTF-8/log_enable=2";

        //VirtDataset dataSet = new VirtDataset(url, "dba", "dba");
        // Model baseModel = dataSet.getNamedModel("http://my.graph.calendar1/");
       // OntModel calendarModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, baseModel);
        OntModel calendarModel = ModelFactory.createOntologyModel();
        //create resources for our two basic classes: Event and Attendee
        Resource ncalEvent = calendarModel.createResource(ncal + "Event");
        // Resource ncalAttendee = calendarModel.createResource(ncal + "Attendee");
        Resource ncalDateTime = calendarModel.createResource(ncal + "NcalDateTime");
        Resource ncalParticipationStatus = calendarModel.createResource(ncal + "ParticipationStatus");
        Resource ncalAttendee = calendarModel.createClass(ncal + "Attendee");
        //create properties
        Property ncalcreated = calendarModel.createDatatypeProperty(ncal + "created");
        Property ncalorganizer = calendarModel.createObjectProperty(ncal + "organizer");
        Property ncaldtstart = calendarModel.createObjectProperty(ncal + "dtstart");
        Property ncaldtend = calendarModel.createObjectProperty(ncal + "dtend");
        Property ncaldateTime = calendarModel.createDatatypeProperty(ncal + "dateTime");
        Property ncalattendee = calendarModel.createObjectProperty(ncal + "attendee");
        Property ncalinvolvedContact = calendarModel.createProperty(ncal + "involvedContact");
        Property ncalpartstat = calendarModel.createDatatypeProperty(ncal + "partstat");
        Property ncalcomment = calendarModel.createDatatypeProperty(ncal + "comment");
        Property ncallocation = calendarModel.createDatatypeProperty(ncal + "location");
        Property ncalsummary = calendarModel.createDatatypeProperty(ncal + "summary");
        Property ncaldescription = calendarModel.createDatatypeProperty(ncal + "description");

        //eventIndividual
        if (event.getCreated() != null) {
            ncalEvent.addProperty(ncalcreated, event.getCreated().toString());
        }
        OntResource organizer = calendarModel.createOntResource(lz + event.getOrganizer().getDisplayName().replaceAll("\\s", ""));
        if (event.getOrganizer() != null) {
            ncalEvent.addProperty(ncalorganizer, organizer);
        }

        if (event.getStart().getDateTime() != null) {
            ncalEvent.addProperty(ncaldtstart, ncalDateTime);
            ncalDateTime.addProperty(ncaldateTime, event.getStart().getDateTime().toString());
        }
        if (event.getEnd().getDateTime() != null) {
            ncalEvent.addProperty(ncaldtend, ncalDateTime);
            ncalDateTime.addProperty(ncaldateTime, event.getEnd().getDateTime().toString());
        }

        //ncalEvent.addProperty(ncalattendee, ncalAttendee);
        if (event.getAttendees() != null) {
            for (int i = 0; i < event.getAttendees().size(); i++) {
                if (event.getAttendees().get(i).getDisplayName() != null) {
                    OntResource attendee = calendarModel.createOntResource(lz + event.getAttendees().get(i).getDisplayName().replaceAll("\\s", ""));

                    //OntResource contact = calendarModel.createOntResource(lz + event.getAttendees().get(i).getDisplayName());
                    ncalEvent.addProperty(ncalattendee, attendee);
                    if (event.getAttendees().get(i).getResponseStatus() != null) {
                        attendee.addProperty(ncalpartstat, event.getAttendees().get(i).getResponseStatus());
                    }
                    if (event.getAttendees().get(i).getComment() != null) {
                        ncalAttendee.addProperty(ncalcomment, event.getAttendees().get(i).getComment());
                    }
                }
            }
        }

        if (event.getLocation() != null) {
            ncalEvent.addProperty(ncallocation, event.getLocation());
        }

        if (event.getSummary() != null) {
            ncalEvent.addProperty(ncalsummary, event.getSummary());
        }

        if (event.getDescription() != null) {
            ncalEvent.addProperty(ncaldescription, event.getDescription());
        }

        return calendarModel;

    }
}
