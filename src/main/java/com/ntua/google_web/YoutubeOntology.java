/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntua.google_web;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.google.api.services.youtube.model.SubscriptionListResponse;
import com.hp.hpl.jena.graph.query.Query;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.vocabulary.DCTypes;
import com.hp.hpl.jena.vocabulary.DC_11;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.XSD;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.List;
import virtuoso.jena.driver.VirtDataset;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 *
 * @author stavroula
 */
public class YoutubeOntology {

    public static String lz = "http://www.linkzoo.gr/#";
    public static String lz_resource = "http://www.linkzoo.gr/resource/";

    public OntModel create(OntModel model, YouTube youtube) throws IOException {
        PlaylistListResponse list;

        ChannelListResponse channelList;

        SubscriptionListResponse subscriptions;
        list = youtube.playlists().list("snippet").setMine(true).execute();
        List<Playlist> playlists = list.getItems();

        channelList = youtube.channels().list("contentDetails,snippet").setMine(true).execute();
        subscriptions = youtube.subscriptions().list("snippet").setMine(true).execute();

        //create basic classes
        OntClass Playlist = model.createClass(lz + "Playlist");
        Playlist.addSuperClass(OWL.Thing);
        Playlist.addLabel("YouTube Playlists", "en");

        OntClass Channel = model.createClass(lz + "Channel");
        Channel.addSuperClass(OWL.Thing);
        Channel.addLabel("YouTubeChannel", "en");

        OntClass MovingImage = model.createClass(lz + "MovingImage");

        //create custom Property
        OntProperty relatedPlaylist = model.createOntProperty(lz + "relatedPlaylist");
        relatedPlaylist.addLabel("Related Playlist", "en");
        relatedPlaylist.addComment("Property for associating playlist to corresponding Channel", "en");
        relatedPlaylist.addDomain(Channel);
        relatedPlaylist.addRange(Playlist);

        Channel.addProperty(relatedPlaylist, Playlist);
        PlaylistItemListResponse videos = new PlaylistItemListResponse();
        //create individual
        for (int i = 0; i < channelList.getItems().size(); i++) {
            Individual channel = Channel.createIndividual(lz + channelList.getItems().get(i).getId());
            channel.addProperty(DC_11.identifier, channelList.getItems().get(i).getId());
            channel.addProperty(DC_11.title, channelList.getItems().get(i).getSnippet().getTitle());
            channel.addProperty(DC_11.description, channelList.getItems().get(i).getSnippet().getDescription());
            channel.addProperty(DCTerms.created, channelList.getItems().get(i).getSnippet().getPublishedAt().toString());
            channel.addProperty(relatedPlaylist, channelList.getItems().get(i).getContentDetails().getRelatedPlaylists().getFavorites());
            channel.addProperty(relatedPlaylist, channelList.getItems().get(i).getContentDetails().getRelatedPlaylists().getLikes());
            channel.addProperty(relatedPlaylist, channelList.getItems().get(i).getContentDetails().getRelatedPlaylists().getWatchLater());
        }

        for (int i = 0; i < list.getItems().size(); i++) {
            Individual playlist = Playlist.createIndividual(lz + list.getItems().get(i).getId());
            playlist.addProperty(DCTerms.created, list.getItems().get(i).getSnippet().getPublishedAt().toString());
            if (list.getItems().get(i).getSnippet().getDescription() != null) {
                playlist.addProperty(DC_11.description, list.getItems().get(i).getSnippet().getDescription());
            }
            if (list.getItems().get(i).getSnippet().getTitle() != null) {
                playlist.addProperty(DC_11.title, list.getItems().get(i).getSnippet().getTitle());
            }
            playlist.addProperty(DC_11.identifier, list.getItems().get(i).getId());
            OntResource channel = model.createOntResource(lz + list.getItems().get(i).getSnippet().getChannelId());
            playlist.addProperty(DCTerms.isPartOf, channel);
            videos = youtube.playlistItems().list("snippet,contentDetails").setPlaylistId(list.getItems().get(i).getId()).execute();
            Individual movingimage = MovingImage.createIndividual(lz + videos.getItems().get(i).getId());
            playlist.addProperty(DCTerms.hasPart, movingimage);
            list.getItems().get(i).getSnippet().getChannelTitle();
        }

        return model;
    }
}
