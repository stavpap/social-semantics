package com.ntua.google_web;

/**
 *
 * @author stavroula
 */
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.util.ServiceException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.google.api.services.plus.Plus;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import com.google.gdata.data.Person;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import virtuoso.jena.driver.VirtDataset;

public class SNCommunicator {

    public int counterContacts, counterEvents = 0;
    public OntModel snUserModel;
    public OntModel calendarModel;
    private static final String CLIENT_ID = "1041410395378-nhf0ngngg9n0ckj5lnm5ois876n4pqhc.apps.googleusercontent.com";
    private static final String CLIENT_SECRET = "ICQCmR72Z4w0rqB8IdZ5OQj9";

    private static final String REDIRECT_URI = "http://localhost:8080/google_web/index.jsp";

    private static final Iterable<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/contacts.readonly;https://www.googleapis.com/auth/calendar.readonly;https://www.googleapis.com/auth/youtube;https://www.googleapis.com/auth/plus.login".split(";"));

    HttpTransport httpTransport = new NetHttpTransport();
    JsonFactory jsonFactory = new JacksonFactory();

    GoogleAuthorizationCodeFlow flow = null;

    private static GoogleCredential credential = null;

    private static YouTube youtube;

    String appName = "Diploma2";

    YoutubeOntology youtubeontology;

    public SNCommunicator() {

        flow = new GoogleAuthorizationCodeFlow.Builder(
                httpTransport, jsonFactory, CLIENT_ID, CLIENT_SECRET, (Collection<String>) SCOPES)
                .setAccessType("online")
                .setApprovalPrompt("auto").build();
    }

    public String redirectToAuthorizationUrl() {

        String url = flow.newAuthorizationUrl().setRedirectUri(REDIRECT_URI).build();

        return url;
    }

    public GoogleCredential getToken(String authcode) throws IOException, ServiceException {

        GoogleTokenResponse response = flow.newTokenRequest(authcode).setRedirectUri(REDIRECT_URI).execute();
        credential = new GoogleCredential().setFromTokenResponse(response);
        return credential;
    }

    public String contactsService() throws ServiceException, IOException {
        ContactsService myContactService = new ContactsService(appName);
        myContactService.setOAuth2Credentials(credential);
        // Request the feed
        URL feedUrl = new URL("https://www.google.com/m8/feeds/contacts/default/full");
        ContactFeed contactsResultFeed = myContactService.getFeed(feedUrl, ContactFeed.class);
        Plus plus = new Plus.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(appName)
                .build();
        com.google.api.services.plus.model.Person mePerson = plus.people().get("me").execute();
        String nameUser = mePerson.getDisplayName();

        FileOutputStream out = null;
        File file;

        try {

            file = new File("c:/user.rdf");
            out = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // Take the info
            for (ContactEntry entry : contactsResultFeed.getEntries()) {
                counterContacts++;

                SnUserOntology snUserontology = new SnUserOntology();
                snUserModel = snUserontology.create(entry, nameUser);
                snUserModel.write(out);
            }
            out.flush();
            out.close();

        } catch (IOException e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
            }
        }
        String numberOfContacts = String.valueOf(counterContacts);

        return numberOfContacts;
    }

    public String calendarService() throws IOException {
        Calendar myCalendarService = new Calendar.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(appName).build();

        // Iterate over the events in the specified calendar
        String pageToken = null;
        do {
            Events events = myCalendarService.events().list("primary").setPageToken(pageToken).execute();

            List<Event> items = events.getItems();
            /*FileOutputStream out = null;
             File file;
             file = new File("c:/calendar.rdf");
             out = new FileOutputStream(file);
             if (!file.exists()) {
             file.createNewFile();
             }*/
            for (Event event : items) {
                counterEvents++;
                CalendarOntology createCalendarOntology = new CalendarOntology();
                calendarModel = createCalendarOntology.create(event);
                //calendarModel.write(out);
            }
            //out.flush();
            //out.close();
            pageToken = events.getNextPageToken();
        } while (pageToken != null);
        return String.valueOf(counterEvents);

    }

    public String youtubeService() throws IOException {
        // This object is used to make YouTube Data API requests.
        youtube = new YouTube.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(appName)
                .build();

        virtuoso();
        //export();

        return "youtube";
    }

    void virtuoso() throws IOException {
        youtubeontology = new YoutubeOntology();

        String url = "jdbc:virtuoso://localhost:1111/autoReconnect=true/charset=UTF-8/log_enable=2";

        VirtDataset dataSet = new VirtDataset(url, "dba", "dba");
        Model baseModel = dataSet.getNamedModel("http://my.graph.youtube1/");

        OntModel youtubeModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, baseModel);
        youtubeontology.create(youtubeModel, youtube);
    }

    void export() throws IOException {
        OntModel youtubeModel = ModelFactory.createOntologyModel();
        //YoutubeOntology youtubeontology = new YoutubeOntology();
        FileOutputStream out = null;
        File file;
        file = new File("c:/youtube1.rdf");
        out = new FileOutputStream(file);
        if (!file.exists()) {
            file.createNewFile();
        }
        youtubeontology.create(youtubeModel, youtube).write(out);
        out.flush();
        out.close();

    }
}
