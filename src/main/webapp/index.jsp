<%@page import="com.ntua.google_web.SNCommunicator"%>
<%@page import="java.util.List"%>
<%@page import="com.google.api.client.googleapis.auth.oauth2.GoogleCredential"%>
<%@page import="com.google.gdata.data.Link"%>
<%@page import="com.google.gdata.data.extensions.ExtendedProperty"%>
<%@page import="com.google.gdata.data.contacts.GroupMembershipInfo"%>
<%@page import="com.google.gdata.data.extensions.Im"%>
<%@page import="com.google.gdata.data.extensions.Email"%>
<%@page import="com.google.gdata.data.contacts.ContactEntry"%>
<%@page import="com.google.gdata.data.contacts.ContactFeed"%>
<%@page import="java.net.URL"%>
<%@page import="com.google.gdata.client.contacts.ContactsService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Social Semantics v1.0 - </title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/custom.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="//platform.linkedin.com/in.js">
            api_key: 771j6qltmifno6
                    onLoad: onLinkedInLoad
            credentials_cookie: true
        </script>
        <script type="text/javascript" src="linkedin.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    </head>
    <body id="page-top" class="index">
        <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Social Semantics</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">Social Semantics</a>
            </div>


        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/logo.jpeg" alt="">
                    <div class="intro-text">
                        <span class="name">Social Semantics</span>
                        <hr class="star-light">
                        <span class="skills">Welcome to Social Semantics<br />
                            You can login with your Google or Linkedin account</span>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-6 text-right">
                        <%
                            String test = "hello";
                            GoogleCredential credential1 = null;
                            final SNCommunicator helper = new SNCommunicator();
                            if (request.getParameter("code") == null) {
                                /**
                                 * initial visit to the page
                                 */
                                out.println("<a href='" + helper.redirectToAuthorizationUrl()
                                        + "' class='btn btn-lg btn-outline'>" + "<i class='fa fa-fw fa-google-plus'></i>"
                                        + "Login</a>");
                            } else if (request.getParameter("code") != null) {
                                credential1 = helper.getToken(request.getParameter("code"));
                                String redirectURL = "home.jsp";
                                response.sendRedirect(redirectURL);
                            }
                        %>
                    </div>
                    <div class="col-lg-6 text-left">
                        <a href="javascript:void(0);" class="btn btn-lg btn-outline" onclick="loginWithLinkedIn();">
                            <i class='fa fa-fw fa-linkedin'></i>
                            Login
                        </a>
                        <div class="hidden">
                            <script type="IN/Login"></script>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Social Semantics 2015
                    </div>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>