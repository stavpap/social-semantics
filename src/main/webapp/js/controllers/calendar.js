'use strict';

var diplomaApp = angular.module('diplomaApp');

diplomaApp.controller('CalendarCtrl', function ($scope, $http) {
    $http.get('/google_web/webresources/service/calendar')
            .success(function (data) {
                $scope.events = data;
            });
                $scope.export = function(){
                    alert('Export your data on rdf/xml format');
                };
});