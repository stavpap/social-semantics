'use strict';

var diplomaApp = angular.module('diplomaApp');

diplomaApp.controller('ContactsCtrl', function ($scope, $http) {
    $http.get('/google_web/webresources/service/contacts')
            .success(function (data) {
                $scope.contacts = data;
$scope.export = function(){
                    alert('Export your data on rdf/xml format');
                };
            });
});

