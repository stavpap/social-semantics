'use strict';

var diplomaApp = angular.module('diplomaApp', [
    'ngRoute'
]);

diplomaApp.config(['$routeProvider', '$sceProvider',
    function ($routeProvider, $sceProvider) {

        $routeProvider.
                when('/home', {
                    templateUrl: 'partials/home.html',
                    controller: 'HomeCtrl'
                }).when('/contacts', {
            templateUrl: 'partials/contacts.html',
            controller: 'ContactsCtrl'
        }).when('/calendar', {
            templateUrl: 'partials/calendar.html',
            controller: 'CalendarCtrl'
        }).when('/youtube', {
            templateUrl: 'partials/youtube.html',
            controller: 'YoutubeCtrl'
        }).
                otherwise({
                    redirectTo: '/home'
                });
        $sceProvider.enabled(false);

    }]);
