<!doctype html>
<html lang="en" ng-app="diplomaApp">
    <head>
        <meta charset="utf-8">
        <title>Diploma</title>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular-route.min.js"></script>
        <script src="https://x2js.googlecode.com/hg/xml2json.js"></script>
        <script src="js/app.js"></script>  
        <script src="js/controllers/home.js"></script>
        <script src="js/controllers/contacts.js"></script>
        <script src="js/controllers/calendar.js"></script>
        <script src="js/controllers/youtube.js"></script>
    </head>
    <body>

        <div ng-view></div>

    </body>
</html>