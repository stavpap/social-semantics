// JavaScript framework is loaded
function onLinkedInLoad() {
    console.log('stavroula');
    IN.Event.on(IN, "auth", onLinkedInAuth);
}


// Viewer has authenticated
function onLinkedInAuth() {
    IN.API.Profile("me").fields("headline", "industry", "summary", "public-profile-url", "first-name", "last-name").result(displayProfiles);
}

// Profile() API call returns successfully
function displayProfiles(profiles) {
    member = profiles.values[0];
    console.log(JSON.stringify(member));
    $.ajax({
        type: "POST",
        data: JSON.stringify(member),
        dataType: "json",
        url: "/google_web/webresources/service/linkedin",
        success: function () {
            console.log('SUCCESS');
        },
        fail: function () {
            console.log('ERROR');
        }
    });
}

function loginWithLinkedIn() {
    $('a[id*=li_ui_li_gen_]').parent().click();
}